<?php

 header("Access-Control-Allow-Origin: *");

//docker conf
//sudo a2enmod rewrite

//user : debian-sys-maint_// root
//mdp sql : c6db75a2287409a86380bbc4f4e25213 (genie urbain) en md5 //root

//sudo dpkg-reconfigure phpmyadmin

  //essai fonction

  function validiteLogin($email, $motdepasse){

    $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("SELECT * FROM compte WHERE email = ?");
    $stmt->execute([$email]);
    $dataCompte = $stmt->fetch();

    if ($email) {
      if ($dataCompte) {
        if ($dataCompte["mdp"] == $motdepasse) {
          return $dataCompte["droit"]; //le compte existe mot de passe bon
        }
        else {
          return 2; //mauvais mot de passe
        }
      }
      else {
        return 1; //le compte existe pas
      }
    }
    else {
      return 0;  //mauvaise requete (vierge)
    }
  }


  /**
  ** Page d'acceuil 🏡
  **/


  require './flight/flight/Flight.php';

  Flight::route('/', function(){
      Flight::json(array('message' => "Bienvenue sur les serveurs d'UtcDocs")); //Message par defaut
  });


  /**
  **  Partie création
  **/

  Flight::route('/creation', function(){

    //demande de donnee
    $email = Flight::request()->data->email;
    $mdp = Flight::request()->data->mdp;
    $typeDemande = Flight::request()->data->typeDemande;


    //verif connexion
    $retourConnexion = validiteLogin($email, $mdp);

    if ($retourConnexion < 3) {
      Flight::json(array('erreur ligne 71' => $retourConnexion, "email" => $email)); //Message d'erreur
    }
    else {

      //Cas sans erreur de connection

      //PDO
      $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      if ($typeDemande == 0 && $retourConnexion == 4) {
        //Demande de creation de categorie en etant connecte
        $informations = Flight::request()->data->informations;
        $nom = $informations['nom'];

        $sql = "INSERT INTO categorie (nom) VALUES (?)";

        if ($nom != "") {
          $conn->prepare($sql)->execute([strval($nom)]);
        }
        Flight::json(array("code" => "200"));
      }

      else if ($typeDemande == 1 && $retourConnexion >= 3) {
        //Demande de creation d'articles provisoires
        $informations = Flight::request()->data->informations;
        $sql = "INSERT INTO articleProvisoire (nomP, catId, description, nomS, imageData, dateArticle, nomCreateur) VALUES (?,?,?,?,?,?,?)";

        if($informations["nomP"] != ""){
          $conn->prepare($sql)->execute([$informations["nomP"], $informations["catId"], $informations["description"], $informations["nomS"], $informations["image"], date('d/m/Y'), $email]);
        }
      }

      else if ($typeDemande == 2 && $retourConnexion == 4) {
        $informations = Flight::request()->data->informations;
        $sql = "INSERT INTO articles (nomP, catId, description, nomS) VALUES (?,?,?,?)";

        if($informations["nomP"] != ""){
          $conn->prepare($sql)->execute([$informations["nomP"], $informations["catId"], $informations["description"], $informations["nomS"]]);
        }
      }

    }

  });


  /**
  **  Partie modification
  **/

    Flight::route('/modification', function(){
      $email = Flight::request()->data->email;
      $mdp = Flight::request()->data->mdp;
      $typeDemande = Flight::request()->data->typeDemande;


      //verif connexion
      $retourConnexion = validiteLogin($email, $mdp);

      if ($retourConnexion != 4) {
        Flight::json(array("erreur" => "access pas assez evolue"));
      }
      else {
        $informations = Flight::request()->data->informations;

        $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if($typeDemande == 0){
          $conn->prepare("UPDATE categorie SET nom=? WHERE id=?")->execute([$informations["nom"], $informations["id"]]);
        }
        elseif ($typeDemande == 1) {
          $conn->prepare("UPDATE articleProvisoire SET nomP=?, nomS=?, description=?, catId=? WHERE id=?")->execute([$informations["nomP"], $informations["nomS"], $informations["description"], $informations["catId"], $informations["id"]]);
        }
        elseif ($typeDemande == 2) {
          $conn->prepare("UPDATE articles SET nomP=?, nomS=?, description=?, catId=? WHERE id=?")->execute([$informations["nomP"], $informations["nomS"], $informations["description"], $informations["catId"], $informations["id"]]);
        }

        Flight::json(array("code" => "200"));
      }
    });



  /**
  **  Partie suppression
  **/

    Flight::route('/suppression', function(){
      $email = Flight::request()->data->email;
      $mdp = Flight::request()->data->mdp;
      $typeDemande = Flight::request()->data->typeDemande;


      //verif connexion
      $retourConnexion = validiteLogin($email, $mdp);

      if ($retourConnexion != 4) {
        Flight::json(array("erreur" => "access pas assez evolue"));
      }
      else {
        $informations = Flight::request()->data->informations;

        $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if($typeDemande == 0){
          $conn->prepare("DELETE FROM categorie WHERE id=?")->execute([$informations["idASupp"]]);
        }
        elseif ($typeDemande == 1) {
          $conn->prepare("DELETE FROM articleProvisoire WHERE id=?")->execute([$informations["idASupp"]]);
        }
        elseif ($typeDemande == 2) {
          $conn->prepare("DELETE FROM articles WHERE id=?")->execute([$informations["idASupp"]]);
        }

        Flight::json(array("code" => $retourConnexion));
      }
    });


    /**
    **  Partie validation
    **/

    Flight::route('/validation', function(){
      $email = Flight::request()->data->email;
      $mdp = Flight::request()->data->mdp;

      //verif connexion
      $retourConnexion = validiteLogin($email, $mdp);

      if ($retourConnexion != 4) {
        Flight::json(array("erreur" => "access pas assez evolue"));
      }
      else {
        $informations = Flight::request()->data->informations;

        $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        $stmt = $conn->query("SELECT * FROM articleProvisoire WHERE id = " . $informations["idAValider"]);
        $dataArt = $stmt->fetch();

        $sql = "INSERT INTO articles (nomP, catId, description, nomS, imageData, dateArticle, nomCreateur) VALUES (?,?,?,?,?,?,?)";
        $conn->prepare($sql)->execute([$dataArt["nomP"], $dataArt["catId"], $dataArt["description"], $dataArt["nomS"], $dataArt["imageData"], $dataArt["dateArticle"], $dataArt["nomCreateur"]]);

        $conn->prepare("DELETE FROM articleProvisoire WHERE id=?")->execute([$informations["idAValider"]]);



        Flight::json(array("code" => $dataArt));
      }
    });


  /**
  **  Partie getter d'information
  **/


  Flight::route('/categories', function(){ //demande toutes les categories

    $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->query("SELECT * FROM categorie");

    $nomsCategorie = array();

    while ($row = $stmt->fetch()) {
      array_push($nomsCategorie, array("id"=>$row["id"], "nom"=>$row["nom"], ));
    }
      Flight::json($nomsCategorie);
  });

  Flight::route('/articlesProvisoires', function(){ //demande tous les articles

    $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->query("SELECT * FROM articleProvisoire");

    $nomsArticle = array();

    while ($row = $stmt->fetch()) {
      array_push($nomsArticle, array("id"=>$row["id"], "catId"=>$row["catId"],  "nomP"=>$row["nomP"],  "nomS"=>$row["nomS"],  "description"=>$row["description"]));
    }
      Flight::json($nomsArticle);
  });


  Flight::route('/articles', function(){ //demande tous les articles

    $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->query("SELECT * FROM articles");

    $nomsArticle = array();

    while ($row = $stmt->fetch()) {
      array_push($nomsArticle, array("id"=>$row["id"], "catId"=>$row["catId"],  "nomP"=>$row["nomP"],  "nomS"=>$row["nomS"]));
    }
      Flight::json($nomsArticle);
  });

  Flight::route('/article/@id', function($id){ //demande tous les articles

    $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->query("SELECT * FROM articles WHERE id = " . $id);

    $data = $stmt->fetch();
      Flight::json(array("nomP" => $data["nomP"], "nomS" => $data["nomS"], "description" => $data["description"], "catId" => $data["catId"], "imageData" => $data["imageData"], "nomCreateur" => $data["nomCreateur"], "dateArticle" => $data["dateArticle"], "id" => $id));
  });

  Flight::route('/categorie/@id', function($id){ //demande tous les articles d'une catégorie
    $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->query("SELECT * FROM articles WHERE catId = " . $id);

    $nomsArticle = array();

    while ($row = $stmt->fetch()) {
      array_push($nomsArticle, array("nomP" => $row["nomP"], "nomS" => $row["nomS"], "description" => $row["description"], "catId" => $row["catId"], "id" => $row["id"]));
    }

    Flight::json($nomsArticle);
  });


  /**
  **  Partie account
  **/

  Flight::route('/compte/creation', function(){ //demande toutes les categories

    $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $informations = Flight::request()->data->informations;
    $sql = "INSERT INTO compte (email, mdp, droit, articles) VALUES (?,?,?,?)";

    if($informations){
      $conn->prepare($sql)->execute([$informations["email"], $informations["mdp"], 3, " "]);
    }

      Flight::json(array("retour" => "bon"));

  });

  Flight::route('/compte/info', function(){ //demande toutes les categories

    $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $informations = Flight::request()->data->informations;

    $retourConnexion = 0;

    if ($informations) {
      $retourConnexion = validiteLogin($informations["email"], $informations["mdp"]);
    }
    Flight::json(array("retour" => $retourConnexion));


  });


  /**
  **  Partie recherche
  **/

  Flight::route('/recherche/@type/@keyword', function($type, $keyword){ //demande toutes les categories

    $conn = new PDO("mysql:host=localhost;dbname=utcDocs", "root", "root");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if($type == "simple"){

      $stmt = $conn->query("SELECT * FROM articles WHERE nomS LIKE '%$keyword%' OR nomP LIKE '%$keyword%' OR description LIKE '%$keyword%' ORDER BY nomP");

      $resultatsRecherche = array();

      $data = array();

      while ($row = $stmt->fetch()) {
        array_push($data, array("nomP" => $row["nomP"], "nomS" => $row["nomS"], "description" => $row["description"], "catId" => $row["catId"], "id" => $row["id"]));
      }



      Flight::json($data);

    }


  });

  Flight::route('/recherche/@type/', function($type){ //demande toutes les categories

    if($type == "simple"){
      Flight::json(array());
    }
  });









  /**
  **  Partie finalisation 🛫
  **/

  Flight::before('json', function () {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE');
    header('Access-Control-Allow-Headers: Content-Type');
});


  Flight::start();

?>
